FROM golang:1.17

ADD ati.tar.gz ./
WORKDIR ati
RUN echo $(ls -1)
RUN go build -o /petshop

EXPOSE 3000

CMD ["/petshop"] 
